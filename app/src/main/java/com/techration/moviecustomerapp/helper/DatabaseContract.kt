package com.techration.moviecustomerapp.helper

import android.net.Uri
import android.provider.BaseColumns
import com.techration.moviecustomerapp.model.FavMovie


class DatabaseContract {

    companion object{
        private val AUTHORITY = "com.roogry.finalandroidexpert"

        class MovieColumns : BaseColumns {
            companion object {
                val CONTENT_URI = Uri.Builder().scheme("content")
                    .authority(AUTHORITY)
                    .appendPath(FavMovie.TABLE_NAME)
                    .build()!!
            }
        }
    }
}