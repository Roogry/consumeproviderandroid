package com.techration.moviecustomerapp.helper

import android.database.Cursor
import com.techration.moviecustomerapp.model.FavMovie


class MappingHelper {
    companion object{
        fun mapCursorToArrayList(moviesCursor: Cursor): ArrayList<FavMovie> {
            val movies: ArrayList<FavMovie> = ArrayList()
            while (moviesCursor.moveToNext()) {
                val id = moviesCursor.getInt(moviesCursor.getColumnIndexOrThrow(FavMovie.COLUMN_ID))
                val title = moviesCursor.getString(moviesCursor.getColumnIndexOrThrow(FavMovie.COLUMN_TITLE))
                val overview = moviesCursor.getString(moviesCursor.getColumnIndexOrThrow(FavMovie.COLUMN_overview))
                val posterPath = moviesCursor.getString(moviesCursor.getColumnIndexOrThrow(FavMovie.COLUMN_posterPath))
                val backdropPath = moviesCursor.getString(moviesCursor.getColumnIndexOrThrow(FavMovie.COLUMN_backdropPath))
                val voteAverage = moviesCursor.getDouble(moviesCursor.getColumnIndexOrThrow(FavMovie.COLUMN_voteAverage))
                movies.add(FavMovie(overview, title, posterPath, backdropPath, voteAverage, id))
            }
            return movies
        }
    }
}