package com.techration.moviecustomerapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.techration.moviecustomerapp.R
import com.techration.moviecustomerapp.model.FavMovie
import kotlinx.android.synthetic.main.movie_item.view.*

class FavMovieAdapter(private val context: Context) :
    RecyclerView.Adapter<FavMovieAdapter.Holder>() {

    private var favMovie: ArrayList<FavMovie> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(parent.context).inflate(R.layout.movie_item, parent, false))
    }

    override fun getItemCount(): Int = favMovie.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(context, favMovie[position])
    }

    fun setMovies(movies: ArrayList<FavMovie>) {
        this.favMovie.clear()
        this.favMovie.addAll(movies)
        notifyDataSetChanged()
    }

    class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(context: Context, favMovie: FavMovie) {

            val sRatting =
                context.resources.getString(context.resources.getIdentifier("rating", "string", context.packageName))
            val rating = StringBuilder().append(sRatting).append(favMovie.voteAverage.toString())
            itemView.txtTitle.text = favMovie.title
            itemView.txtRating.text = rating
            itemView.txtOverview.text = favMovie.overview

            Glide.with(context)
                .load("https://image.tmdb.org/t/p/w185${favMovie.posterPath}")
                .into(itemView.mvPoster)

        }
    }
}