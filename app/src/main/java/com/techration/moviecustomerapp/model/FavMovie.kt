package com.techration.moviecustomerapp.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import android.provider.BaseColumns

@Parcelize
data class FavMovie(
    var overview: String? = null,
    var title: String? = null,
    var posterPath: String? = null,
    var backdropPath: String? = null,
    var voteAverage: Double? = null,
    var id: Int ?= null
) : Parcelable{

    companion object{
        const val TABLE_NAME = "movies"
        const val COLUMN_ID = BaseColumns._ID
        const val COLUMN_TITLE = "title"
        const val COLUMN_posterPath = "poster_path"
        const val COLUMN_backdropPath = "backdrop_path"
        const val COLUMN_voteAverage = "vote_average"
        const val COLUMN_overview = "overview"

    }

}