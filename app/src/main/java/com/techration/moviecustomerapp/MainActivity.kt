package com.techration.moviecustomerapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.techration.moviecustomerapp.adapter.FavMovieAdapter
import com.techration.moviecustomerapp.helper.DatabaseContract.Companion.MovieColumns.Companion.CONTENT_URI
import com.techration.moviecustomerapp.helper.MappingHelper.Companion.mapCursorToArrayList
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.doAsync

class MainActivity : AppCompatActivity() {
    private lateinit var adapter: FavMovieAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        actionBar?.title = "Movies"

        adapter = FavMovieAdapter(this)
        rv.layoutManager = LinearLayoutManager(this)
        rv.adapter = adapter

        getMovies()
    }

    private fun getMovies(){
        doAsync{
            val movies = applicationContext.contentResolver.query(CONTENT_URI, null, null, null, null)!!
            val listMovies = mapCursorToArrayList(movies)
            if (listMovies.size > 0) {
                adapter.setMovies(listMovies)
            } else {
                adapter.setMovies(ArrayList())
            }
        }
    }
}
